========================
File Extension Converter
========================

This README file is for the File Extension Converter project.

Introduction
============

The File Extension Converter project is a console application built using the `.NET <https://dotnet.microsoft.com/download/dotnet/6.0>`_ framework (version 6.0).

This application can be used to convert file extensions in a specified directory.

Project Creation
================

This section describes how the project was created and some of the initial setup.
    
1. Create the project using the **Console App** template available via the **New Project** UI in Visual Studio.
2. Use **file-extension-converter** for the project name.
3. Select **.NET 6.0** as the target framework.

*NOTE:* The project could also be created by using the ``dotnet`` CLI tool, via the following command:

::

    dotnet new console --framework net6.0 --name file-extension-converter

Making Changes
==============

The procedure for making changes is:

1. Create a fork of the project. This will be your own personal repo.
2. Clone your personal repo. Create a feature branch off the main development branch.
3. Make your changes.
4. Stage, commit, and push your changes - using the feature branch - to your personal repo.
5. Create a merge request in GitLab to merge changes from your personal repo into the official project.

*NOTE:* Keep the main development branch stable. Never push broken/non-working/half-baked changes to the main development branch.

Building the Application
========================

This section contains information on how to build/publish the application. This is most relevant to build engineers.

Preparing a Build Environment to Build .NET Projects
----------------------------------------------------

Prepare a machine - such as a virtual machine - to run the builds on.

*NOTE:* This machine should have a clean/fresh installation of the operating system. We want to avoid having existing/overlapping dependencies affecting our builds.

`Download <https://dotnet.microsoft.com/download>`_ and install the 64-bit version of the .NET SDK. This will include the dotnet CLI used to build .NET projects.

Verify that the .NET SDK is installed properly by running the following command: ``dotnet --info``

Building the Project
--------------------

To build the project using the ``dotnet`` CLI tool, run the following command:

::

    dotnet build [PROJECT FILE PATH] --configuration [CONFIGURATION] --runtime [RUNTIME IDENTIFIER] --nologo --verbosity normal

Parameters explained:

* ``[PROJECT FILE PATH]`` - The project file of the target project.
* ``--configuration`` - This sets the build configuration.
* ``[CONFIGURATION]`` - The configuration of the build.
* ``--runtime`` - This sets the target runtime.
* ``[RUNTIME IDENTIFIER]`` - The `runtime identifier <https://docs.microsoft.com/en-us/dotnet/core/rid-catalog>`_.
* ``--nologo`` - This suppresses the startup banner or copyright message in the build output.
* ``--verbosity`` - This sets the verbosity of MSBuild in the build output.

*Example:*

::

    dotnet build "C:\path\to\my\project.csproj" --configuration Release --runtime win-x64 --nologo --verbosity normal

Build Output
------------

**TODO: Describe the build output. Describe each artifact without being pedantic.**

Publishing the Project
----------------------

It should be published as a single executable. This can be done using the following command:

::

    dotnet publish [PROJECT FILE PATH] --configuration [CONFIGURATION] --runtime [RUNTIME IDENTIFIER] --output [OUTPUT DIRECTORY] --self-contained true -p:PublishSingleFile=true --nologo --verbosity normal

Parameters explained:

* ``[PROJECT FILE PATH]`` - The project file of the target project.
* ``--configuration`` - This sets the build configuration.
* ``[CONFIGURATION]`` - The configuration of the build.
* ``--runtime`` - This sets the target runtime. *NOTE:* This is required for a `self-contained deployment <https://docs.microsoft.com/en-us/dotnet/core/deploying/#publish-self-contained>`_.
* ``[RUNTIME IDENTIFIER]`` - The `runtime identifier <https://docs.microsoft.com/en-us/dotnet/core/rid-catalog>`_.
* ``--output`` - Specifies the output directory to publish to.
* ``[OUTPUT DIRECTORY]`` - Where the application will be published to.
* ``--self-contained`` - Indicates whether the application will be published as a `self-contained deployment <https://docs.microsoft.com/en-us/dotnet/core/deploying/#publish-self-contained>`_.
* ``-p:PublishSingleFile=true`` - This condenses the application to a single file.
* ``--nologo`` - This suppresses the startup banner or copyright message in the build output.
* ``--verbosity`` - This sets the verbosity of the publish output.

*Example:*

::

    dotnet publish "C:\path\to\my\project.csproj" --configuration Release --runtime win-x64 --output "\\network\share\" --self-contained true -p:PublishSingleFile=true --nologo --verbosity normal

Publish Output
--------------

**TODO: Describe the publish output. Describe each artifact without being pedantic.**

References
----------

* `Documentation for the dotnet CLI. <https://docs.microsoft.com/en-us/dotnet/core/tools/>`_
* `User-friendly download page for .NET. <https://dotnet.microsoft.com/download>`_
* `Installation Overview page for .NET. <https://docs.microsoft.com/en-us/dotnet/core/install/>`_
* `List of the different ways of installing the .NET SDK. <https://docs.microsoft.com/en-us/dotnet/core/install/sdk>`_
* `List of the basic requirements and dependencies for .NET. <https://docs.microsoft.com/en-us/dotnet/core/install/windows#dependencies>`_
* `Document showing how to verify what versions of .NET are installed. <https://docs.microsoft.com/en-us/dotnet/core/install/how-to-detect-installed-versions>`_
* `Advanced download page for .NET 6. <https://dotnet.microsoft.com/download/dotnet/6.0>`_

Development
===========

Setup
-----

**TODO: Describe how to setup an environment for developing the application.**

Run
---

**TODO: Describe the different options for running the application.**

Debug
-----

**TODO: Describe the different options for debugging the application.**

Deployment
==========

Because this is a simple tool, there's no deployment procedure.

Application Configuration
=========================

Parameters:

* **target-directory** - The directory to process. By default, the application processes the current directory.
* **traverse-deep** - Indicates whether to process just the files in the target directory or all files - including sub-directories. Specify ``false``/``0`` (default) for the former and ``true``/``1`` for the latter.
* **old-extension** - The file extension filter. Files with this extension will be processed.
* **new-extension** - The resulting file extension. Files that are processed will have their extensions changed to this.

*NOTE:* Trailing periods (.) are removed from file extension parameters.

*NOTE:* The file extension filter parameter does not support preceding periods (.).

Parameters can either be provided at the command line or in a configuration file. The configuration file should be named ``file-extension-converter.ini`` and be present in the same directory as the application executable. It's contents should look similar to the following:

::

	[General]
	target-directory = C:\Users\foo\bar
	traverse-deep = false
	old-extension = foo
	new-extension = bar

*NOTE:* Command line parameters take precedence over configuration file parameters.

After processing, the parameters used will be written to a configuration file for convenience.

*Example:*

::

	file-extension-converter.exe --target-directory=C:\Users\foo\bar --traverse-deep=true --old-extension=bar --new-extension=foo

References
==========

**TODO: Include helpful references.**
