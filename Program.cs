﻿using IniParser.Model;
using IniParser;
using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;

namespace FileExtensionConverter
{
    public class Program
    {
        public static void Main(string[] commandLineArguments)
        {
            // TODO: Review OSI-approved licenses. Pick one for this project.

            ShowIntroduction();

            // Get the configuration file parameters.
            FileExtensionConverterParameters? configurationFileParameters = null;
            try
            {
                configurationFileParameters = GetConfigurationFileParameters();
            }
            catch (Exception exception)
            {
                Console.WriteLine($"Failed to get the configuration file parameters.\n\n{exception}\n\nContinuing without file configuration.");
            }

            // Get the command line parameters.
            Dictionary<string, string?> rawCommandLineParameters = GetRawCommandLineParameters(commandLineArguments);
            FileExtensionConverterParameters? commandLineParameters = null;
            try
            {
                commandLineParameters = GetCommandLineParameters(rawCommandLineParameters);
            }
            catch (Exception exception)
            {
                Console.WriteLine($"Failed to get the command line parameters.\n\n{exception}");
                return;
            }

            // NOTE: Originally, I planned on validating the target directory. But apparently that's not a trivial task. Instead, I choose to catch any exceptions thrown during parameter parsing and the conversion process.

            FileExtensionConverterParameters parameters = GetCombinedParameters(configurationFileParameters, commandLineParameters);

            ShowParameters(parameters);

            try
            {
                ConvertFileExtensions(parameters);
            }
            catch (Exception exception)
            {
                Console.WriteLine($"Failed to convert file extensions.\n\n{exception}");
                return;
            }

            try
            {
                CreateConfigurationFile(parameters);
            }
            catch (Exception exception)
            {
                Console.WriteLine($"Failed to create or write to the configuration file.\n\n{exception}");
            }

            // TODO: Convert this console application into a desktop application. Make sure it has:
            //     Browse button - to choose the target directory.
            //     Checkbox - to enable deep traversal of target directory.
            //     Input field - old extension.
            //     Input field - new extension.
            //     Switch button - to swap values of extension input fields.
            //     Button - to initiate conversion process.
            //     Output field - to show the description, affected files, warnings, and errors.
            // Increase the version of the project to 2.0 when it becomes a UI application.
        }

        private static void ShowIntroduction()
        {
            Console.WriteLine("+--------------------------------------+");

            Console.WriteLine($"\nThis is the File Extension Converter console application. Version: {Assembly.GetEntryAssembly().GetName().Version}.\n\nThis simple utility converts file extensions in a specified directory.");

            string parametersDescription = "\nParameters:";
            parametersDescription += "\n* target-directory - The directory to process. By default, the application processes the current directory.";
            parametersDescription += "\n* traverse-deep - Indicates whether to process just the files in the target directory or all files - including sub-directories. Specify false/0 (default) for the former and true/1 for the latter.";
            parametersDescription += "\n* old-extension - The file extension filter. Files with this extension will be processed.";
            parametersDescription += "\n* new-extension - The resulting file extension. Files that are processed will have their extensions changed to this.";

            Console.WriteLine(parametersDescription);

            Console.WriteLine("\nNOTE: Trailing periods (.) are removed from file extension parameters.");
            Console.WriteLine("NOTE: The file extension filter parameter does not support preceding periods (.).");

            Console.WriteLine("\nParameters can either be provided at the command line or in a configuration file. The configuration file should be named \"file-extension-converter.ini\" and be present in the same directory as the application executable. It's contents should look similar to the following:\n");

            string configurationFile = "\t[General]";
            configurationFile += "\n\ttarget-directory = C:\\Users\\foo\\bar";
            configurationFile += "\n\ttraverse-deep = false";
            configurationFile += "\n\told-extension = foo";
            configurationFile += "\n\tnew-extension = bar";

            Console.WriteLine(configurationFile);

            Console.WriteLine("\nNOTE: Command line parameters take precedence over configuration file parameters.");

            Console.WriteLine("\nAfter processing, the parameters used will be written to a configuration file for convenience.");

            string example = "\nExample:";
            example += "\n\n\tfile-extension-converter.exe --target-directory=C:\\Users\\foo\\bar --traverse-deep=true --old-extension=bar --new-extension=foo";

            Console.WriteLine(example);

            Console.WriteLine("\n+--------------------------------------+");
        }

        private static FileExtensionConverterParameters GetConfigurationFileParameters()
        {
            FileExtensionConverterParameters result = new FileExtensionConverterParameters();

            FileIniDataParser iniConfigurationFileParser = new FileIniDataParser();
            IniData configurationData = iniConfigurationFileParser.ReadFile("file-extension-converter.ini");

            string targetDirectory = configurationData["General"]["target-directory"];
            if (!string.IsNullOrEmpty(targetDirectory))
            {
                result.TargetDirectory = targetDirectory;
            }

            string? traverseDeepRaw = configurationData["General"]["traverse-deep"];
            if (traverseDeepRaw != null)
            {
                if (string.Equals(traverseDeepRaw, "true", StringComparison.OrdinalIgnoreCase) || string.Equals(traverseDeepRaw, "1", StringComparison.OrdinalIgnoreCase))
                {
                    result.TraverseDeep = true;
                }
                else if (string.Equals(traverseDeepRaw, "false", StringComparison.OrdinalIgnoreCase) || string.Equals(traverseDeepRaw, "0", StringComparison.OrdinalIgnoreCase))
                {
                    result.TraverseDeep = false;
                }
            }

            string? oldExtension = configurationData["General"]["old-extension"];
            if (oldExtension != null)
            {
                // Remove trailing periods (.).
                oldExtension = oldExtension.TrimEnd('.');

                // TODO: Change the file conversion process to handle extensions containing periods (.) - e.g. --old-extension=foo.bar. To do this, we'll probably have to manually get the file's extension - as opposed to using the Path.GetExtension method.
                if (!string.IsNullOrEmpty(oldExtension) && oldExtension.Contains("."))
                {
                    throw new Exception($"This application does not currently support extension filters containing periods (.). The old extension value - {oldExtension} - is invalid.");
                }

                result.OldExtension = oldExtension;
            }

            string? newExtension = configurationData["General"]["new-extension"];
            if (newExtension != null)
            {
                // Remove trailing periods (.).
                newExtension = newExtension.TrimEnd('.');

                result.NewExtension = newExtension;
            }

            return result;
        }

        private static Dictionary<string, string?> GetRawCommandLineParameters(string[] commandLineArguments)
        {
            Dictionary<string, string?> result = new Dictionary<string, string?>();

            foreach (string commandLineArgument in commandLineArguments)
            {
                if (string.IsNullOrEmpty(commandLineArgument))
                    continue;

                if (!commandLineArgument.StartsWith("--", System.StringComparison.OrdinalIgnoreCase))
                    continue;

                string parameterAndValueRaw = commandLineArgument.Substring(2);

                string[] parameterAndValue = parameterAndValueRaw.Split('=', 2);

                string? parameter = null;
                if (parameterAndValue.Length > 0)
                    parameter = parameterAndValue[0];

                if (string.IsNullOrEmpty(parameter))
                    continue;

                string? value = null;
                if (parameterAndValue.Length == 2)
                    value = parameterAndValue[1];

                if (result.ContainsKey(parameter))
                    result[parameter] = value;
                else
                    result.Add(parameter, value);
            }

            return result;
        }

        private static FileExtensionConverterParameters GetCommandLineParameters(Dictionary<string, string?> rawParameters)
        {
            FileExtensionConverterParameters result = new FileExtensionConverterParameters();

            if (rawParameters == null)
            {
                throw new ArgumentNullException(nameof(rawParameters));
            }

            string parameterName = "target-directory";
            if (rawParameters.ContainsKey(parameterName))
            {
                string targetDirectory = rawParameters[parameterName] ?? "";
                if (!string.IsNullOrEmpty(targetDirectory))
                {
                    result.TargetDirectory = targetDirectory;
                }
            }

            parameterName = "traverse-deep";
            if (rawParameters.ContainsKey(parameterName))
            {
                string? traverseDeepRaw = rawParameters[parameterName];
                if (traverseDeepRaw != null)
                {
                    if (string.Equals(traverseDeepRaw, "true", StringComparison.OrdinalIgnoreCase) || string.Equals(traverseDeepRaw, "1", StringComparison.OrdinalIgnoreCase))
                    {
                        result.TraverseDeep = true;
                    }
                    else if (string.Equals(traverseDeepRaw, "false", StringComparison.OrdinalIgnoreCase) || string.Equals(traverseDeepRaw, "0", StringComparison.OrdinalIgnoreCase))
                    {
                        result.TraverseDeep = false;
                    }
                }
            }

            parameterName = "old-extension";
            if (rawParameters.ContainsKey(parameterName))
            {
                string? oldExtension = rawParameters[parameterName];
                if (oldExtension != null)
                {
                    // Remove trailing periods (.).
                    oldExtension = oldExtension.TrimEnd('.');

                    // TODO: Change the file conversion process to handle extensions containing periods (.) - e.g. --old-extension=foo.bar. To do this, we'll probably have to manually get the file's extension - as opposed to using the Path.GetExtension method.
                    if (!string.IsNullOrEmpty(oldExtension) && oldExtension.Contains("."))
                    {
                        throw new Exception($"This application does not currently support extension filters containing periods (.). The old extension value - {oldExtension} - is invalid.");
                    }

                    result.OldExtension = oldExtension;
                }
            }

            parameterName = "new-extension";
            if (rawParameters.ContainsKey(parameterName))
            {
                string? newExtension = rawParameters[parameterName];
                if (newExtension != null)
                {
                    // Remove trailing periods (.).
                    newExtension = newExtension.TrimEnd('.');

                    result.NewExtension = newExtension;
                }
            }

            return result;
        }

        private static FileExtensionConverterParameters GetCombinedParameters(FileExtensionConverterParameters? configurationFileParameters, FileExtensionConverterParameters? commandLineParameters)
        {
            if (commandLineParameters == null)
            {
                throw new ArgumentNullException(nameof(commandLineParameters));
            }

            FileExtensionConverterParameters result = new FileExtensionConverterParameters();
            result.TargetDirectory = Directory.GetCurrentDirectory();
            result.TraverseDeep = false;

            if (configurationFileParameters != null)
            {
                if (!string.IsNullOrEmpty(configurationFileParameters.TargetDirectory))
                {
                    result.TargetDirectory = configurationFileParameters.TargetDirectory;
                }

                if (configurationFileParameters.TraverseDeep != null)
                {
                    result.TraverseDeep = configurationFileParameters.TraverseDeep;
                }

                if (configurationFileParameters.OldExtension != null)
                {
                    result.OldExtension = configurationFileParameters.OldExtension;
                }

                if (configurationFileParameters.NewExtension != null)
                {
                    result.NewExtension = configurationFileParameters.NewExtension;
                }
            }

            if (!string.IsNullOrEmpty(commandLineParameters.TargetDirectory))
            {
                result.TargetDirectory = commandLineParameters.TargetDirectory;
            }

            if (commandLineParameters.TraverseDeep != null)
            {
                result.TraverseDeep = commandLineParameters.TraverseDeep;
            }

            if (commandLineParameters.OldExtension != null)
            {
                result.OldExtension = commandLineParameters.OldExtension;
            }

            if (commandLineParameters.NewExtension != null)
            {
                result.NewExtension = commandLineParameters.NewExtension;
            }

            return result;
        }

        private static void ShowParameters(FileExtensionConverterParameters parameters)
        {
            string message = "\nProcessing with the following parameters and values:";
            message += $"\n* target-directory = {parameters.TargetDirectory}";
            message += $"\n* traverse-deep = {parameters.TraverseDeep}";
            message += $"\n* old-extension = {parameters.OldExtension}";
            message += $"\n* new-extension = {parameters.NewExtension}";
            message += "\n";

            Console.WriteLine(message);
        }

        private static void ConvertFileExtensions(FileExtensionConverterParameters parameters)
        {
            if (parameters == null)
            {
                throw new ArgumentNullException(nameof(parameters));
            }

            if (string.IsNullOrEmpty(parameters.TargetDirectory))
            {
                throw new Exception("The target directory parameter is missing.");
            }

            if (parameters.TraverseDeep == null)
            {
                throw new Exception("The deep-traversal parameter is missing.");
            }

            if (parameters.OldExtension == null)
            {
                throw new Exception("The old extension parameter is missing.");
            }

            if (parameters.NewExtension == null)
            {
                throw new Exception("The new extension parameter is missing.");
            }

            // TODO: Consider implementing multi-threaded/parallel logic to increase throughput. Test if this makes any difference in performance.

            // NOTE: This application does not consider the period (.) preceding the file extension to be a part of the file extension. It is merely a separator between the file name and the file extension.

            string targetDirectory = parameters.TargetDirectory;
            bool traverseDeep = (bool)parameters.TraverseDeep;
            string oldExtension = parameters.OldExtension;
            string newExtension = parameters.NewExtension;

            string searchPattern = "";
            if (!string.IsNullOrEmpty(oldExtension))
                searchPattern = $"*.{oldExtension}";
            string[] files = Directory.GetFiles(targetDirectory, searchPattern, traverseDeep ? SearchOption.AllDirectories : SearchOption.TopDirectoryOnly);
            foreach (string filePath in files)
            {
                string? fileDirectory = Path.GetDirectoryName(filePath);
                if (string.IsNullOrEmpty(fileDirectory))
                    continue;

                string fileExtension = Path.GetExtension(filePath);
                if (fileExtension == null)
                    continue;

                // Remove the period (.) from the file extension.
                if (!string.IsNullOrEmpty(fileExtension) && fileExtension.Length > 1)
                    fileExtension = fileExtension.Substring(1);

                // If the old extension is empty, only process files lacking extensions.
                if (string.IsNullOrEmpty(oldExtension))
                    if (!string.IsNullOrEmpty(fileExtension))
                        continue;

                if (!string.Equals(fileExtension, oldExtension, StringComparison.Ordinal))
                    continue;

                string fileNameSansExtension = Path.GetFileNameWithoutExtension(filePath);
                string newFileName = "";
                if (!string.IsNullOrEmpty(newExtension))
                    newFileName = $"{fileNameSansExtension}.{newExtension}";
                else
                    newFileName = fileNameSansExtension;
                string newFilePath = Path.Combine(fileDirectory, newFileName);

                if (string.Equals(filePath, newFilePath, StringComparison.Ordinal))
                    continue;

                Console.WriteLine($"{filePath} --> {newFilePath}");

                File.Move(filePath, newFilePath);
            }
        }

        private static void CreateConfigurationFile(FileExtensionConverterParameters parameters)
        {
            if (parameters == null)
            {
                throw new ArgumentNullException(nameof(parameters));
            }

            FileIniDataParser iniConfigurationFileParser = new FileIniDataParser();
            CreateConfigurationFile();
            IniData configurationData = iniConfigurationFileParser.ReadFile("file-extension-converter.ini");

            if (!string.IsNullOrEmpty(parameters.TargetDirectory))
            {
                configurationData["General"]["target-directory"] = parameters.TargetDirectory;
            }

            if (parameters.TraverseDeep != null)
            {
                configurationData["General"]["traverse-deep"] = (bool)parameters.TraverseDeep ? "true" : "false";
            }

            if (parameters.OldExtension != null)
            {
                configurationData["General"]["old-extension"] = parameters.OldExtension;
            }

            if (parameters.NewExtension != null)
            {
                configurationData["General"]["new-extension"] = parameters.NewExtension;
            }

            iniConfigurationFileParser.WriteFile("file-extension-converter.ini", configurationData);
        }

        private static void CreateConfigurationFile()
        {
            if (!File.Exists("file-extension-converter.ini"))
            {
                using (File.Create("file-extension-converter.ini"))
                {
                }
            }
        }
    }

    internal class FileExtensionConverterParameters
    {
        public string TargetDirectory = "";
        public bool? TraverseDeep = null;
        public string? OldExtension = null;
        public string? NewExtension = null;

        public FileExtensionConverterParameters()
        {
        }
    }
}